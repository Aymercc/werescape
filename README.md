Jeu créé lors du cours -Principes de conception et de développement de jeux vidéos- proposé à l'UQAC et encadré par Bruno BOUCHARD.


Afin de lancer le jeu, rendez-vous dans le dossier WereScape\Assets\Builds\
Puis exécutez l'application WereScape.exe



Développé par Lucas FILLEUL, Aymeric LE MOAL et Vincent VANBALBERGHE.

