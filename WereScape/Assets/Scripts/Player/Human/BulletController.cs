﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    private Rigidbody2D rb;         // Le RigidBody du projectile

    public float speed;             // La vitesse du projectile
    public float lifetime = 3f;     // La duree de vie
    public int damage;              // Les dégats du projectile

    private void Start()
    {
        rb = gameObject.GetComponentInChildren<Rigidbody2D>();
        rb.velocity = -transform.up * speed;
    }

    private void Update()
    {
        lifetime -= Time.deltaTime;
        if (lifetime <= 0f)
        {
            Destroy(gameObject);    
        }
    }

    public void Die()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Wall") || collision.CompareTag("Obstacle"))
        {
            Die();
        }
        if (collision.CompareTag("Scientist"))
        {
            collision.GetComponent<ScientistController>().GetHit(damage);
            Die();
        }
        if (collision.CompareTag("Military"))
        {
            collision.GetComponent<MilitaryController>().GetHit(damage);
            Die();
        }
    }
}
