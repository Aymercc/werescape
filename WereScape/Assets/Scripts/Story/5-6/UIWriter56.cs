﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIWriter56 : MonoBehaviour
{
    private Text StoryText;                               // Variable contenant sur le texte du niveau

    private TextWriter.TextWriterSingle textWriterSingle; // Permet de gérer plusieurs writers en même temps

    public TextWriter writer;                            // Référence sur le text writer pour la musique

    // Références sur les 3 boutons de choix
    public GameObject Choice01;
    public GameObject Choice02;
    public GameObject Choice03;

    private int state;                                     // Permet de savoir où on en est dans la scène

    private void Awake()
    {
        StoryText = transform.Find("Canvas").Find("TransparentPanel").Find("StoryText").GetComponent<Text>();
    }

    private void HideChoices()
    {
        Choice01.SetActive(false);
        Choice02.SetActive(false);
        Choice03.SetActive(false);
    }

    private void ShowChoices()
    {
        Choice01.SetActive(true);
        Choice02.SetActive(true);
    }

    private void ShowChoice()
    {
        Choice03.SetActive(true);
    }

    private void Start()
    {
        if (GlobalVariables.forme == GlobalVariables.Formes.ALPHA)
        {
            if (GlobalStory.freePrisoner)
            {
                Choice01.GetComponentInChildren<Text>().text = "Tuer";
                Choice02.GetComponentInChildren<Text>().text = "Recruter";
                string message = GlobalStory.story56A_start_free;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoices);
                state = 0;
            }
            else
            {
                GlobalStory.killPrisoner = true;
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Combatre";
                string message = GlobalStory.story56A_start_leave;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                state = 0;
            }
        }
        else if (GlobalVariables.forme == GlobalVariables.Formes.WereWolf)
        {
            if (GlobalStory.freePrisoner)
            {
                Choice01.GetComponentInChildren<Text>().text = "Tuer";
                Choice02.GetComponentInChildren<Text>().text = "Rejoindre";
                string message = GlobalStory.story56LG_start_free;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoices);
                state = 0;
            }
            else
            {
                GlobalStory.killPrisoner = true;
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Combatre";
                string message = GlobalStory.story56LG_start_leave;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                state = 0;
            }
        }
        else if (GlobalVariables.forme == GlobalVariables.Formes.YoungWolf)
        {
            if (GlobalStory.freePrisoner)
            {
                Choice01.GetComponentInChildren<Text>().text = "Tuer";
                Choice02.GetComponentInChildren<Text>().text = "Laisser";
                string message = GlobalStory.story56JL_start_free;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoices);
                state = 0;
            }
            else
            {
                GlobalStory.killPrisoner = true;
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Combatre";
                string message = GlobalStory.story56JL_start_leave;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                state = 0;
            }
        }
        else
        {
            if (GlobalStory.freePrisoner)
            {
                Choice01.GetComponentInChildren<Text>().text = "Tuer";
                Choice02.GetComponentInChildren<Text>().text = "Laisser";
                string message = GlobalStory.story56H_start_free;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoices);
                state = 0;
            }
            else
            {
                GlobalStory.killPrisoner = true;
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Combatre";
                string message = GlobalStory.story56H_start_leave;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                state = 0;
            }
        }
    }

    public void Choice1()
    {
        if (state == 0)
        {
            GlobalStory.killPrisoner = true;
            if (GlobalVariables.forme == GlobalVariables.Formes.ALPHA)
            {
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Combattre";
                string message = GlobalStory.story56A_1A;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                ++state;
            }
            else if (GlobalVariables.forme == GlobalVariables.Formes.WereWolf)
            {
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Combattre";
                string message = GlobalStory.story56LG_1A;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                ++state;
            }
            else if (GlobalVariables.forme == GlobalVariables.Formes.YoungWolf)
            {
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Combattre";
                string message = GlobalStory.story56JL_1A;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                ++state;
            }
            else
            {
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Combattre";
                string message = GlobalStory.story56H_1A;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                ++state;
            }
        }
    }

    public void Choice2()
    {
        if (state == 0)
        {
            GlobalVariables.humanity += 5;
            if (GlobalVariables.forme == GlobalVariables.Formes.ALPHA)
            {
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Continuer";
                string message = GlobalStory.story56A_1B;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                ++state;
            }
            else if (GlobalVariables.forme == GlobalVariables.Formes.WereWolf)
            {
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Continuer";
                string message = GlobalStory.story56LG_1B;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                ++state;
            }
            else if (GlobalVariables.forme == GlobalVariables.Formes.YoungWolf)
            {
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Continuer";
                string message = GlobalStory.story56JL_1B;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                ++state;
            }
            else
            {
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Continuer";
                string message = GlobalStory.story56H_1B;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                ++state;
            }
        }
    }

    public void Choice3()
    {
        if (state <= 1 && GlobalStory.killPrisoner && !GlobalStory.prisonerDead)
        {
            if (GlobalVariables.forme == GlobalVariables.Formes.ALPHA)
            {
                GlobalStory.prisonerDead = true;
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Continuer";
                string message = GlobalStory.storyCombatA;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                ++state;
            }
            else if (GlobalVariables.forme == GlobalVariables.Formes.WereWolf)
            {
                GlobalStory.prisonerDead = true;
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Continuer";
                string message = GlobalStory.storyCombatLG;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                ++state;
            }
            else if (GlobalVariables.forme == GlobalVariables.Formes.YoungWolf)
            {
                GlobalStory.prisonerDead = true;
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Continuer";
                string message = GlobalStory.storyCombatJL;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                ++state;
            }
            else
            {
                GlobalStory.prisonerDead = true;
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Continuer";
                string message = GlobalStory.storyCombatH;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                ++state;
            }
        }
        else // Transformation en fonction de l'humanité si elle atteint un certain seuil
        {
            if (GlobalVariables.humanity >= GlobalVariables.evolutionYoungWolf &&
                GlobalVariables.humanity < GlobalVariables.evolutionWereWolf &&
                GlobalVariables.forme != GlobalVariables.Formes.YoungWolf)
            {
                GlobalVariables.forme = GlobalVariables.Formes.YoungWolf;
                Choice03.GetComponentInChildren<Text>().text = "Continuer";
                string message = GlobalStory.transfo_young;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
            }
            else if (GlobalVariables.humanity >= GlobalVariables.evolutionWereWolf &&
                GlobalVariables.humanity < GlobalVariables.evolutionALPHA &&
                GlobalVariables.forme != GlobalVariables.Formes.WereWolf)
            {
                GlobalVariables.forme = GlobalVariables.Formes.WereWolf;
                Choice03.GetComponentInChildren<Text>().text = "Continuer";
                string message = GlobalStory.transfo_wolf;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
            }
            else if (GlobalVariables.humanity >= GlobalVariables.evolutionALPHA &&
                GlobalVariables.forme != GlobalVariables.Formes.ALPHA)
            {
                GlobalVariables.forme = GlobalVariables.Formes.ALPHA;
                Choice03.GetComponentInChildren<Text>().text = "Continuer";
                string message = GlobalStory.transfo_alpha;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
            }
            else
            {
                LoadNextScene();
            }
        }
    }

    public void LoadNextScene()
    {
        writer.FadeToZero();
        //Lancement de la scene suivante
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
