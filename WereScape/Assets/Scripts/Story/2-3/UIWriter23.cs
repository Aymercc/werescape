﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIWriter23 : MonoBehaviour
{
    private Text StoryText;                               // Variable contenant sur le texte du niveau

    private TextWriter.TextWriterSingle textWriterSingle; // Permet de gérer plusieurs writers en même temps

    public TextWriter writer;                            // Référence sur le text writer pour la musique

    // Références sur les 3 boutons de choix
    public GameObject Choice01;
    public GameObject Choice02;
    public GameObject Choice03;

    private int state;                                     // Permet de savoir où on en est dans la scène

    private void Awake()
    {
        StoryText = transform.Find("Canvas").Find("TransparentPanel").Find("StoryText").GetComponent<Text>();
    }

    private void HideChoices()
    {
        Choice01.SetActive(false);
        Choice02.SetActive(false);
        Choice03.SetActive(false);
    }

    private void ShowChoices()
    {
        Choice01.SetActive(true);
        Choice02.SetActive(true);
    }

    private void ShowChoice()
    {
        Choice03.SetActive(true);
    }

    private void Start()
    {
        Choice01.GetComponentInChildren<Text>().text = "Prendre";
        Choice02.GetComponentInChildren<Text>().text = "Détruire";
        string message = GlobalStory.story23_start;
        textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoices);
        state = 0;
    }

    public void Choice1()
    {
        if (state == 0)
        {
            if (GlobalStory.freePrisoner)
            {
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Je suis prêt !";
                string message = GlobalStory.story23_1A_free;
                GlobalVariables.chargeurUpgrade = true;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                ++state;
            }
            else
            {
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Je suis prêt !";
                string message = GlobalStory.story23_1A_leave;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                ++state;
            }
        }
    }

    public void Choice2()
    {
        if (state == 0)
        {
            GlobalVariables.humanity += 5;
            if (GlobalStory.freePrisoner)
            {
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Je suis prêt !";
                string message = GlobalStory.story23_1B_free;
                GlobalVariables.chargeurUpgrade = true;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                ++state;
            }
            else
            {
                HideChoices();
                Choice03.GetComponentInChildren<Text>().text = "Je suis prêt !";
                string message = GlobalStory.story23_1B_leave;
                textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                ++state;
            }
        }
    }

    public void Choice3()
    {
        // Texte de transformation si l'humanité atteint un certain seuil
        if (GlobalVariables.humanity >= GlobalVariables.evolutionYoungWolf &&
            GlobalVariables.humanity < GlobalVariables.evolutionWereWolf &&
            GlobalVariables.forme != GlobalVariables.Formes.YoungWolf)
        {
            GlobalVariables.forme = GlobalVariables.Formes.YoungWolf;
            Choice03.GetComponentInChildren<Text>().text = "Continuer";
            string message = GlobalStory.transfo_young;
            textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
        }
        else if (GlobalVariables.humanity >= GlobalVariables.evolutionWereWolf &&
            GlobalVariables.humanity < GlobalVariables.evolutionALPHA &&
            GlobalVariables.forme != GlobalVariables.Formes.WereWolf)
        {
            GlobalVariables.forme = GlobalVariables.Formes.WereWolf;
            Choice03.GetComponentInChildren<Text>().text = "Continuer";
            string message = GlobalStory.transfo_wolf;
            textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
        }
        else
        {
            LoadNextScene();
        }
    }

    public void LoadNextScene()
    {
        writer.FadeToZero();
        //Lancement de la scene suivante
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
