﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasController : MonoBehaviour
{
    public Camera humanCam;             // Référence sur la caméra de la forme humaine
    public Camera youngWolfCam;         // Référence sur la caméra de la forme de jeune loup
    public Camera wolfCam;              // Référence sur la caméra de la forme de loup
    public Camera alphaCam;             // Référence sur la caméra de la forme d'alpha
    public GameObject humanHud;         // Référence sur le HUD de la forme humaine
    public GameObject youngWolfHud;     // Référence sur le HUD de la forme de jeune loup
    public GameObject wolfHud;          // Référence sur le HUD de la forme de loup
    public GameObject alphaHud;         // Référence sur le HUD de la forme d'Alpha
    private PlayerController player;    // Référence sur le script du joueur
    private Canvas canvas;              // Référence sur le canvas
    public Color rgbBackgroundColor;    // Couleur du background, pas utilisé dans la version actuelle

    // La fonction ModifyCanvas() est appelée lorsque la transformation a été effectuée dans TransformationController
    // afin d'afficher uniquement le HUD et la caméra correspondants à la forme actuelle
    public void ModifyCanvas()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        canvas = gameObject.GetComponent<Canvas>();

        if (player.forme == "Human")
        {
            canvas.worldCamera = humanCam;
            youngWolfHud.GetComponent<ActivateDeactivate>().Deactivate();
            wolfHud.GetComponent<ActivateDeactivate>().Deactivate();
            alphaHud.GetComponent<ActivateDeactivate>().Deactivate();
          
        }
        else if (player.forme == "YoungWolf")
        {
            canvas.worldCamera = youngWolfCam;
            humanHud.GetComponent<ActivateDeactivate>().Deactivate();
            wolfHud.GetComponent<ActivateDeactivate>().Deactivate();
            alphaHud.GetComponent<ActivateDeactivate>().Deactivate();
        }
        else if (player.forme == "Wolf")
        {
            canvas.worldCamera = wolfCam;
            youngWolfHud.GetComponent<ActivateDeactivate>().Deactivate();
            humanHud.GetComponent<ActivateDeactivate>().Deactivate();
            alphaHud.GetComponent<ActivateDeactivate>().Deactivate();
        }
        else if (player.forme == "ALPHA")
        {
            canvas.worldCamera = alphaCam;
            youngWolfHud.GetComponent<ActivateDeactivate>().Deactivate();
            humanHud.GetComponent<ActivateDeactivate>().Deactivate();
            wolfHud.GetComponent<ActivateDeactivate>().Deactivate();
        }
    }
}
