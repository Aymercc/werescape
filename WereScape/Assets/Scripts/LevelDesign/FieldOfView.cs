﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{

    private Mesh mesh;                                  // Cette classe va permettre de crée le champ de vision
    private Vector3 origin;                             // pour celà, nous allons crée un grand nombre de triangles 
    private float startingAngle;                        // connexes partant tous de la même origine.
    private float fov;                          
    private float fovMax;                       

    public LayerMask layerMask;
    private float viewDistance;
    private float viewDistanceMax;

    public Material empty_material;
    public Material maskMaterial;

    private void Start()
    {
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;
        origin = Vector3.zero;
        fov = 60f;
        fovMax = fov;
        viewDistance = 4f;
        viewDistanceMax = viewDistance;
    }

    private void LateUpdate()
    {
        int rayCount = 50;                              //Un grand nombre de triangle va permettre de faire un bord "arondi" à notre champ de vision.
        float angle = startingAngle; 
        float angleIncrease = fov / rayCount;

        Vector3[] vertices = new Vector3[rayCount + 1 + 1];
        Vector2[] uv = new Vector2[vertices.Length];
        int[] triangles = new int[rayCount * 3];

        vertices[0] = origin;


        int vertexIndex = 1;
        int triangleIndex = 0;
        for (int i=0; i<= rayCount; i++)                //Création des triangles formant le champ de vision
        {
            Vector3 vertex;
            RaycastHit2D raycastHit2D = Physics2D.Raycast(origin, GetVectorFromAngle(angle), viewDistance, layerMask); //Crée un raycast pour chaques triangles composant le champ de vision
            


            if (raycastHit2D.collider == null)          // Si le raycast ne rencontre pas d'objets particuliers alors nous pouvons set les triangles normalement.
            {
                vertex = origin + GetVectorFromAngle(angle) * viewDistance;
            }
            else
            {
                vertex = raycastHit2D.point;            // Dans le cas contraire, nous allons réduire la taille des triangles pour que le champ de vision ne passe pas au travers des objets rencontrés.

            }

            vertices[vertexIndex] = vertex;
            if (i > 0)
            {
                triangles[triangleIndex + 0] = 0;       //Relie les 3 sommets du triangle courrant.
                triangles[triangleIndex + 1] = vertexIndex - 1;
                triangles[triangleIndex + 2] = vertexIndex;
                triangleIndex += 3;
            }


            vertexIndex++;                              // Passe au vertex suivant.        
            angle -= angleIncrease;                     // Change l'angle pour que le triangle suivant soit connexe et non superposé au dernier.

        };



        mesh.vertices = vertices;                       // Set les triangles formés si dessus dans le mesh.
        mesh.uv = uv;
        mesh.triangles = triangles;
        mesh.bounds = new Bounds(origin, Vector3.one * 1000f);



    }

    // Cette fonction permet de récupérer un vecteur à partir d'un angle, elle à été récuperée du code open source de CodeMonkey (Youtube)
    private Vector3 GetVectorFromAngle(float angle)
    {
        float angleRad = angle * (Mathf.PI / 180f);
        return new Vector3(Mathf.Cos(angleRad), Mathf.Sin(angleRad));
        
    }

    // Cette fonction permet de récupérer un angle à partir d'un vecteur, elle à été récuperée du code open source de CodeMonkey (Youtube)
    private float GetAngleFromVectorFloat(Vector3 dir)
    {
        dir = dir.normalized;
        float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        if (n < 0) n += 360;

        return n;
    }


    public void SetOrigin(Vector2 origin)
    {
        this.origin = origin;
    }
    public void SetAimDirection(Vector2 aimDirection)
    {
        startingAngle = GetAngleFromVectorFloat(aimDirection) + fov / 2f;
    }
    public void TurnOffLight()
    {
        GetComponent<MeshRenderer>().material = empty_material;
    }
    public void TurnOnLight()
    {
        viewDistance = viewDistanceMax;
        GetComponent<MeshRenderer>().material = maskMaterial;
    }
    public void SetDistance(float distance)
    {
        this.viewDistance = distance;
    }
    public float GetViewDistance()
    {
        return viewDistance;
    }
    public float GetFov()
    {
        return fov;
    }
    public float GetViewDistanceMax()
    {
        return viewDistanceMax;
    }
    public float GetFovMax()
    {
        return fovMax;
    }

}
