﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class ScientistController : MonoBehaviour
{
    public float speed;                     // La vitesse du scientifique
    public int damage;                      // Les dégats du scientifique

    private Transform playerPos;            // La position du joueur pour pouvoir le suivre
    private Vector2 direction;              // La direction vers laquelle regarde (position du joueur)
    private PlayerController player;        // Le script du joueur pour pouvoir lui enlever de la vie

    private SpawnerController spawner;      // La référence sur le script du spawner pour pouvoir faire baisser le nombre de scientifique qu'il reste quand il meurt
    private AudioSource hitSound;

    public int health;                      // La vie du scientifique

    public GameObject deathEffect;          // Effet à la mort stylé ou quoi

    private Rigidbody2D rb;

    private bool moving = true;             // Pour l'arrêter assez loin du joueur pour pas qu'ils se ratatinent dessus
    private GameObject[] walls;             // Référence sur tous les murs de l'arène

    private bool attack = false;            // Pour passer en mode attaque
    public float startTimeBtwAttacks;       // Temps entre chaque attaque
    private float timeBtwAttacks;           // timer du temps entre chaque attaque

    public float startTimeBeforeDying;      // Le temps avant qu'il meure à 0hp
    private float timeBeforeDying = 0;      // Le temps avant qu'il meure

    private bool isDidacticiel = false;

    // Pour les piles
    public int batteryDropRate;             // Les chances pour drop une pile sont de 1/batteryDropRate
    public GameObject battery;              // Une référence sur le gameObject de la pile

    // Pour empêcher de prendre 2 fois les dégats vu qu'il a 2 collider
    private float timeBtwGettingHit;
    private float startTimeBtwGettingHit = 0.1f;

    private SpriteRenderer spriteRenderer;      // Pour le passage en rouge quand il se fait toucher
    public Animator animator;                   // L'animateur de notre objet

    //A*
    public float nextWaypointDistance = 1f; // distance avant de passer au waypoint suivant
    private Path path;                      // Une variable qui contient le chemin actuel
    private int currentWaypoint = 0;        // index du waypoint actuel
    private Seeker seeker;                  // référence sur le component seeker de l'objet
    public float updatePathRate = 0.1f;     // Temps entre chaque rafraichissement des chemins

    private bool dead = false;


    private void Start()
    {
        // Récupération des références
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        hitSound = GetComponentInChildren<AudioSource>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        spawner = GameObject.FindGameObjectWithTag("Spawner").GetComponent<SpawnerController>();
        seeker = GetComponent<Seeker>();

        timeBtwAttacks = startTimeBtwAttacks;

        //On récupère les murs et on ignore les collisions avec eux
        walls = GameObject.FindGameObjectsWithTag("Wall");
        for (int i = 0; i < walls.Length; i++)
        {
            Physics2D.IgnoreCollision(walls[i].GetComponent<Collider2D>(), gameObject.GetComponent<Collider2D>());
        }

        InvokeRepeating("UpdatePath", 0f, updatePathRate);     // Permet de lancer la fonction UpdatePath tous les updatePathRate

        // Pour que les ennemis ne poussent pas le joueur
        Physics2D.IgnoreCollision(GameObject.FindGameObjectWithTag("Player").GetComponent<Collider2D>(), gameObject.GetComponent<Collider2D>());

        // On vérifie si ce sont les ennemis du didacticiel
        if (transform.parent != null)
            if (transform.parent.name == "Ennemies")
                isDidacticiel = true;
    }

    private void FixedUpdate()
    {
        // Si c'est le didacticiel, le scientifique ne doit pas bouger
        if (isDidacticiel)
        {
            moving = false;
            direction = -((Vector2)playerPos.position - (Vector2)transform.position).normalized;
            animator.SetFloat("Horizontal", -direction.x);
            animator.SetFloat("Vertical", -direction.y);
        }

        animator.SetBool("Moving", moving);
        if (playerPos != null && moving == true && dead == false)          // Pour pas tout casser quand le joueur meurt
        {
            // Calcul de la direction qu'il faut regarder
            direction = -((Vector2)playerPos.position - (Vector2)transform.position).normalized;

            if (path == null)
                return;

            if (currentWaypoint >= path.vectorPath.Count)
                return;

            //Calcul d'où aller puis création de la force
            Vector2 whereGo = ((Vector2)path.vectorPath[currentWaypoint] - rb.position).normalized;
            Vector2 force = whereGo * speed * Time.deltaTime;

            animator.SetFloat("Horizontal", -direction.x);
            animator.SetFloat("Vertical", -direction.y);

            rb.AddForce(force);

            float distance = Vector2.Distance(rb.position, path.vectorPath[currentWaypoint]);

            if (distance < nextWaypointDistance)
            {
                currentWaypoint++;
            }
        }
        else if (player != null && attack == true)              // Permet d'attaquer en continu en fonction du temps de rafraichissement de l'attaque
        {
            if (timeBtwAttacks < startTimeBtwAttacks)
            {
                timeBtwAttacks -= Time.deltaTime;
                if (timeBtwAttacks <= 0)
                {
                    timeBtwAttacks = startTimeBtwAttacks;
                }
            }
            else if (timeBtwAttacks == startTimeBtwAttacks)
            {
                player.GetHit(damage);
                timeBtwAttacks -= Time.deltaTime;
            }
        }

        if (timeBtwGettingHit != startTimeBtwGettingHit)    // frame d'invincibilité
        {
            timeBtwGettingHit -= Time.deltaTime;
            if (timeBtwGettingHit <= 0)
                timeBtwGettingHit = startTimeBtwGettingHit;
        }

        // Temps avant de mourir pour pouvoir jouer le son
        if (timeBeforeDying > 0)
        {
            timeBeforeDying -= Time.deltaTime;
            if (timeBeforeDying <= 0)
            {
                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if(isDidacticiel)
                Physics2D.IgnoreCollision(GameObject.FindGameObjectWithTag("Player").GetComponent<Collider2D>(), gameObject.GetComponent<Collider2D>());
            moving = false;
            attack = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            moving = true;
            attack = false;
        }
    }

    public void Die()
    {
        if (isDidacticiel)                                                          // Si le militaire meurt pendant le didacticiel, cela influe l'humanité du joueur
        {
            GlobalVariables.humanity += 3;
        }

        Instantiate(deathEffect, transform.position, Quaternion.identity);          // On crée l'effet de particule

        if (player.forme == "Human")                                                // Si le joueur est encore sous forme humaine, alors il y a une chance de laisse tomber une batterie
        {
            int randPos = Random.Range(0, batteryDropRate);
            if (randPos == 0)
                Instantiate(battery, transform.position, Quaternion.identity);
        }

        spawner.DeathOfScientist();
        timeBeforeDying = startTimeBeforeDying;
        dead = true;
    }

    public void GetHit(int damage)
    {
        if (timeBtwGettingHit == startTimeBtwGettingHit)
        {
            hitSound.Play();
            health -= damage;
            StartCoroutine("HitAnimation");
            if (health <= 0 && !dead)
            {
                Die();
            }
            timeBtwGettingHit -= Time.deltaTime;
        }
    }

    private IEnumerator HitAnimation()
    {
        spriteRenderer.color = Color.red;
        yield return new WaitForSeconds(startTimeBtwGettingHit);
        spriteRenderer.color = Color.white;
    }

    private void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }

    private void UpdatePath()
    {
        if (seeker.IsDone())        // On vérifie s'il est pas déjà en train de calculer un chemin
        {
            seeker.StartPath(rb.position, playerPos.position, OnPathComplete);
        }
    }
}
